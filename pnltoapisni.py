import email
import re
import os
import sys
import imaplib
import smtplib
import ctypes
import getpass
import quopri
import telegram
import pdfkit
import datetime
from itertools import tee
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import formatdate
from email import encoders
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
from email.header import decode_header
from time import sleep
import openpyxl
from openpyxl import load_workbook

mail = imaplib.IMAP4_SSL('imap.gmail.com',993)
unm = 'apis@arubaairlines.aw'
pwd = 'aruba297!!'
mail.login(unm,pwd)
mail.select("INBOX")
mga_contacts=['efrain.valles@arubaairlines.aw',
              'jefeturno@migob.gob.ni',
              'gsergio.jose@hotmail.com',
              'jose.otano@arubaairlines.aw',
              'kzuniga@eaai.com.ni',
              'max.figuera@arubaairlines.aw',
              'faguerrero@eaai.com.ni',
              'roger3ss@hotmail.com',
              'ITStaff@arubaairlines.aw',
              'dta@inac.gob.ni',
              'estudiose@inac.gob.ni',
              'analisis.areodgme@migob.gob.ni',
              'effie.jayx@gmail.com',
              'franklyn.krosendijk@arubaairlines.aw',
              'evalles@arubaairlines.aw',
              'carlos.parra@arubaairlines.aw',
              'edwin.gonzalez@arubaairlines.aw',
              'francisco.arendsz@arubaairlines.aw',
              'occ@arubaairlines.aw',
              'iain.schwengle@arubaairlines.aw',
              'havmga@arubaairlines.aw'
              ]

test_contacts=['efrain.valles@arubaairlines.aw',
              'voices.in@gmail.com',
              'apis@arubaairlines.aw',
              'effie.jayx@gmail.com']

mailtothis = 'efrain.valles@arubaairlines.com'
#mailtothis = 'pnl_aru@airline-choice.com'
server = smtplib.SMTP('smtp.gmail.com:587')
server.starttls()
server.login(unm,pwd)
#bot = telegram.Bot(token="394211218%3AAAF7EfA7GHw6nccUJQIUhIGlegQTWXgkBMg")
chat_id = "14185356"
def loop():
    mail.select('INBOX')
    n=0
    seat=""
    pnr=""
    bags=""
    (retcode ,messages)=mail.search(None,'(UNSEEN)')
    if retcode == 'OK':
        #bot.send_message(chat_id=chat_id,text="OK", parse_mode=telegram.ParseMode.MARKDOWN)
        for num in messages[0].split():
            n +=1
            print(n)
            typ, data = mail.fetch(num, '(RFC822)')
            raw_email = data[0][1]
            raw_email_string = raw_email.decode('utf-8')
            email_message = email.message_from_string(raw_email_string)
            email_from = email.utils.parseaddr(email_message['From'])[1]
            subject = str(email.header.make_header(email.header.decode_header(email_message['Subject'])))
            if "PNL AG930" in subject :
                print(subject)
                pre,fnp = subject.split("PNL", 1)
                fnp=fnp.split()
                fname = fnp[0].replace('/','_')
                vuelo,fecha =re.split('_',fname)
                print (vuelo,fecha)
                fecha = fecha+str(datetime.datetime.now().year)
                listadf = []
                for part in email_message.walk():
                   if part.get_content_maintype() == 'multipart':
                       #body=part.get_payload(0)
                       #print(body)
                       body2=str(part.get_payload(0))
                       pnlfile = 'PNL%s.txt' % (fname)
                       newpnl = open(pnlfile,'w')
                       newpnl.write(body2)
                       newpnl.close()
                       #print (body2)
                       #print(type(body2))
                       #body2 = "\n".join(filter(None, body2.split("\n")[3:]))
                       #with body2.splitlines() as f:
                       it0, it1 = tee(open(pnlfile))
                       next(it1)
                       for val0, val1 in zip(it0, it1):
                           #print (val0+val1)
                           if val0.startswith(".R/DOCS") and val1.startswith(".RN/"):
                               param, value = val0.split("DOCS HK1",1)
                               param2, value2 = val1.split(".RN/",1)
                               truevalue=value.strip('\n')+value2
                               #print(truevalue)
                               valuesplit = re.split(r'/', truevalue)
                               if len(valuesplit)==11:
                                   valuesplit[-2] = valuesplit[-2].strip('\n')+" "+valuesplit[-1].strip('\n')
                                   valuesplit.pop()
                                   #print("%d %s" %(len(valuesplit),type(valuesplit)))
                                   #headvalue=valuesplit[-1].strip('\n')
                                   #print(headvalue)
                                   #valuenew= headvalue+tailvalue[0]
                                   #valuenew=valuenew.replace('/',' ')
                                   #print (valuenew)
                                   #valuesplit.pop()
                                   #valuesplit.append(valuenew)
                               #print (valuesplit)
                               #for s, i in zip(valuesplit, range(len(valuesplit))):
                                   #print (s,i)
                               listadf.append((valuesplit[8].strip('\n')+"/"+valuesplit[9].strip('\n'),valuesplit[4],valuesplit[3],valuesplit[5]))
                           elif val0.startswith(".R/DOCS") and val1.startswith("1"):
                               param, value = val0.split("DOCS HK1",1)
                               truevalue=value.strip('\n')
                               valuesplit = re.split(r'/', truevalue)
                               listadf.append((valuesplit[8].strip('\n')+"/"+valuesplit[9].strip('\n'),valuesplit[4],valuesplit[3],valuesplit[5]))

                           elif val0.startswith(".R/DOCS") and val1.startswith("ENDPNL"):
                               param, value = val0.split("DOCS HK1",1)
                               truevalue=value.strip('\n')
                               valuesplit = re.split(r'/', truevalue)
                               listadf.append((valuesplit[8].strip('\n')+"/"+valuesplit[9].strip('\n'),valuesplit[4],valuesplit[3],valuesplit[5]))
                                   #names = "%s" % next(f)
                                   #if names.startswith(".RN/"):
                                       #namess = names.split(".RN/",1)
                                       #newdocs = value+namess
                                       #print (newdocs) #value = re.split(r'/', value)
                       
                       preapisfile= r'PRE APIS %s %s.xlsx' % (vuelo,fecha)
                       titulos=['Nombres y Apellidos','Nacionalidad','Pasaporte','Fecha de Nacimiento']
                       apisnidf = pd.DataFrame.from_records(listadf, columns=titulos) 
                       srcfile = openpyxl.load_workbook('atn.xlsx',read_only=False)
                       sheetname = srcfile.get_sheet_by_name('Sheet1')
                       srcfile.save(preapisfile)
                       book = load_workbook(preapisfile)
                       writer = pd.ExcelWriter(preapisfile, engine='openpyxl')
                       writer.book = book
                       writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
        #finaldf.to_excel(writer, sheet_name='Sheet 1', )
                       apisnidf.index += 1
                       apisnidf.reset_index()
                       apisnidf.to_excel(writer, sheet_name='Sheet1', index=True, startcol=0,startrow=1)
                       print (apisnidf)
                       writer.save()
                       continue

                   if part.get('Content-Disposition') is None:
                       continue
                   #body = part.get_payload(0)
                   #print(body)
                   #Cleantext = BeautifulSoup(body,"html.parser").text
                   #body = "\n".join(filter(None, Cleantext.split("\n")[3:]))
                   for line in body:
                       #print (line)
                       if line.startswith(".R/DOCS"):
                           param, value = line.split("DOCS HK1",1)
                           value = re.split(r'/', value)
                           names = "%s" % next(f)
                           if names.startswith(".RN/"):
                               namess = names.split(".RN/",1)
                               newdocs = value+namess
                               print (newdocs) #value = re.split(r'/', value)
				#print(value)
                               #list_apis.append([pnr,namessplit[0],namessplit[1].strip(),seat,bags,value[4],value[3],value[5]])
                   break
            if "PRL AG930" in subject  :
                print("detectado")
                print(subject)
                pre,fnp = subject.split("PRL", 1)
                fnp=fnp.split()
                fname = fnp[0].replace('/','_')
                vuelo,fecha =re.split('_',fname)
                fecha = fecha+str(datetime.datetime.now().year)
                for part in email_message.walk():
                    if part.get_content_maintype() == 'multipart':
                        print(part.get_payload(0))
                        continue
                    else:
                        print(part.get_payload(None, True))
                    if part.get('Content-Disposition') is None:
                        continue
                    fileName = part.get_filename()
                    #print(fileName)
                    if bool(fileName):
                        filePath = os.path.join('.',fileName)
                        if not os.path.isfile(filePath) :
                            fp = open(filePath, 'wb')
                            fp.write(part.get_payload(decode=True))
                            #print(type(fp))
                            fp.close()
                            list_apis2 = []
                            list_apis = []
                            #print(textodeemail)
                            #testprl = testprl.replace('HAV','AUA')
                            #send_body ='\n'.join(textodeemail.split('\n')[3:-20])
                            with open(filePath) as f:
                                for line in f:
                                    if line[0]=="1":
                                        bagsearch = re.search("\.W/K/(.+?) ",line)
                                        if bagsearch:
                                            bags = bagsearch.groups()[0]
                                        else:
                                            bags = ""
                                        pnrsearch = re.search("\.L/(.+?) ",line)
                                        if pnrsearch:
                                            pnr = pnrsearch.groups()[0]
                                            #print (pnr)
                                        else:
                                            pnr=""
                                        seatsearch = re.search("\.R/SEAT HK1 (.+?) ",line)
                                        if seatsearch:
                                            seat = seatsearch.groups()[0]
                                            #print (seat)
                                        else:
                                            seatline = "%s" % next(f)
                                            seatagain = re.search("\.R/SEAT HK1 (.+?) ",seatline)
                                            seat=seatagain.groups()[0]

                                    if line.startswith(".R/DOCS"):
                                        param, value = line.split("DOCS HK1",1)
                                        value = re.split(r'/', value)
                                        names = "%s" % next(f)
                                        if names.startswith(".RN/"):
                                            namess = names.split(".RN/",1)
                                        elif names.startswith("1"):
                                            namess = names.split("1",1)
                                        namessplit = re.split(r'/',namess[1])
                                        #print(value)
                                        list_apis.append([pnr,namessplit[0],namessplit[1].strip(),seat,bags,value[4],value[3],value[5]])
                                        list_apis2.append((namessplit[1].strip()+"/"+namessplit[0].strip(),value[4],value[3],value[5]))
                                    elif line.startswith(".R/PSPT"):
                                        param, value = line.split("PSPT HK1",1)
                                        value = re.split(r'/',value)
                                        list_apis2.append((value[4].strip()+"/"+value[3].strip(),value[1],value[0],value[2]))
                                        list_apis.append(["**INF**",value[3],value[4],'**INF**','**INF**',value[1],value[0],value[2]])
                            finalapisfile= r'APIS FINAL %s %s.xlsx' % (vuelo,fecha)
                            titulos=['Nombres y Apellidos','Nacionalidad','Pasaporte','Fecha de Nacimiento']
                            finalapisnidf = pd.DataFrame.from_records(list_apis2, columns=titulos) 
                            srcfile = openpyxl.load_workbook('atn.xlsx',read_only=False)
                            sheetname = srcfile.get_sheet_by_name('Sheet1')
                            srcfile.save(finalapisfile)
                            book = load_workbook(finalapisfile)
                            writer = pd.ExcelWriter(finalapisfile, engine='openpyxl')
                            writer.book = book
                            writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
        #finaldf.to_excel(writer, sheet_name='Sheet 1', )
                            finalapisnidf.index += 1
                            finalapisnidf.reset_index()
                            finalapisnidf.to_excel(writer, sheet_name='Sheet1', index=True, startcol=0,startrow=1)
                            print (finalapisnidf)
                            writer.save()
                            data_to_send = pd.DataFrame(list_apis,columns=['PNR','LASTNAME','FIRST NAME','SEAT','BAGS','NATIONALITY','DOCS','DOB'])
                            data_to_send.index += 1
                            data_to_send.replace(np.nan, '', regex=True)
                            #data_to_send['letter'] = data_to_send['SEAT'].str.extract('([A-Z]\w{0,})', expand=True)
                            #data_to_send['num'] = data_to_send['SEAT'].str.extract('([0-9])', expand=True)
                            #data_to_send['num']=data_to_send['num'].astype(int)
                            #data_to_send.to_excel(r'APIS %s.xlsx' % fname, index=None, header=True)
                            #data_to_send = data_to_send.sort_values(['num','letter'], ascending=[True,True])
                            data_to_send = data_to_send.sort_values('LASTNAME', ascending=True)
                            #print (data_to_send.num.dtype)
                            #data_to_send.drop(['num', 'letter'], axis=1, inplace=True)
                            data_to_send.reset_index()
                            data_to_send.to_html('test.html')
                            with open('test.html', 'r') as original: data = original.read()
                            with open('test.html', 'w') as modified: modified.write ("<html><head><link rel='stylesheet' type='text/css' href='table.css' media='screen' /> <body><header><div class='logoag'></div><div class='flightinfo'><h2>Passenger Manifest</h2><h2>Flight:"+vuelo+" Date:"+fecha+"  </h2></div></header>"+data+"</body></html>")

                            PdfFilename=r'Manifiesto PAX %s %s.pdf' % (vuelo,fecha)
                            pdfkit.from_file('test.html', PdfFilename)
                            #break
                            print (data_to_send.shape[0])

                            server = smtplib.SMTP('smtp.gmail.com:587')
                            server.ehlo()
                            server.starttls()
                            server.login(unm,pwd)
                            msg=MIMEMultipart()
                            msg['From'] = "apis@arubaairlines.aw"
                            msg['To'] = "apis@arubaairlines.aw"
                            msg['Date'] = formatdate(localtime = True)
                            msg['Subject'] = "Apis Final %s " % fname
                            msg.attach(MIMEText("Saludos, \n Anexo APIS Finales vuelo %s" % fname))
                            
                            part2 = MIMEBase('application', "octet-stream")
                            part2.set_payload(open(finalapisfile, "rb").read())
                            encoders.encode_base64(part2)
                            part2.add_header('Content-Disposition', 'attachment; filename="APIS Finales %s.xlsx"' % fname)
                            msg.attach(part2)
                            msgit = "\r\n".join([
                                "From: apis@arubaairlines.aw",
                                "To: efrain.valles@arubaairlines.aw",
                                "Subject: APIS Test",
                                "",
                                "test",
                                ])
#                            server.sendmail('apis@arubaairlines.aw', 'efrain.valles@arubaairlines.aw', msg.as_string())
                            for i in mga_contacts:
                                server.sendmail('apis@arubaairlines.aw', i, msg.as_string())
                            msg2=MIMEMultipart()
                            msg2['From'] = "apis@arubaairlines.aw"
                            msg2['To'] = "apis@arubaairlines.aw"
                            msg2['Date'] = formatdate(localtime = True)
                            msg2['Subject'] = "Manifiesto PAX %s " % fname
                            msg2.attach(MIMEText("Saludos, \n Anexo Manifiesto del Vuelo %s" % fname)) 
                            part3 = MIMEBase('application', "octet-stream")
                            part3.set_payload(open(PdfFilename, "rb").read())
                            encoders.encode_base64(part3)
                            part3.add_header('Content-Disposition', 'attachment; filename="Manifiesto PAX %s.pdf"' % fname)
                            msg2.attach(part3)
                            msgit2 = "\r\n".join([
                                "From: apis@arubaairlines.aw",
                                "To: efrain.valles@arubaairlines.aw",
                                "Subject: Manifiesto de pasajeros %s %s" % (vuelo,fecha),
                                "",
                                "Manifiesto de pasajeros %s %s" % (vuelo,fecha),
                                ])
                            for i in mga_contacts:
                                server.sendmail('apis@arubaairlines.aw', i, msg2.as_string())
                            server.quit()
                            #os.remove(fileName)
                            break 
            #for part in email_message.walk():
                #body = part.get_payload()
                #print(body)    
            #print(part)
            #replaced = body.replace('HAV  AUA  MGA','AUA  MGA')
            #cleantext = BeautifulSoup(body).text
            #body = "\n".join(filter(None,cleantext.split("\n")[3:]))
                #textodeemail = "%s" % body[0]
                #print(textodeemail)
                #textodeemail = textodeemail.replace(' HAV  AUA  MGA',' AUA  MGA')
                #textodeemail = textodeemail.replace('HAV','AUA')
                #send_body ='\n'.join(textodeemail.split('\n')[3:-20])
                #print(send_body)
                #break
            #print (send_body)

                #server = smtplib.SMTP('smtp.gmail.com:587')
                #server.ehlo()
                #server.starttls()
                #server.login(unm,pwd)
                #msgit = "\r\n".join([
                #   "From: apis@arubaairlines.aw",
                #   "To: pnl_aru@airline-choice.com",
                #   "Subject: APIS Test",
                #   "",
                #   "%s" % send_body,
                #   ])
                #server.sendmail('apis@arubaairlines.aw', mailtothis, msgit)
                #server.quit()
                #break
                #texto = """*%s*\n%s""" % (subject,body)
                #print(body),subject,email_from)
            	#bot.send_message(chat_id=chat_id,text=texto,parse_mode=telegram.ParseMode.MARKDOWN)
            for respone_part in data:
                 if isinstance (respone_part, tuple):
                     original = email.message_from_string(respone_part[1].decode())
                     #print original['From']
                     data = original['Subject']
                     #print quopri.decodestring(data)
                     typ, data = mail.store(num,'+FLAGS','\\Seen')
            continue
    #print(n)


if __name__ == '__main__':
    try:
        while True:
            loop()
    finally:
        print ("Thanks")

